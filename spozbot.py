import discord
import logging
import asyncio
from requests import get
from discord.ext import commands
from random import randint, choice

logging.basicConfig(level=logging.INFO)

bot = commands.Bot(command_prefix='!')

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')

@bot.command()
async def support(ctx):
    support = choice(["It's probably your internet conection!",
        "Have you tried turning it off and on again?",
        "I believe the technical term is ... It's fucked.",
        "It's a feature, not a fault.",
        "Try re-aligning the flux capacitor.",
        "The problem seems to exist between the chair and the keyboard"])
    await ctx.send(support)

@bot.command()
async def ip(ctx):
    ip = get('https://ident.me/').text
    await ctx.send(ip+':65535')

@bot.command()
async def roll(ctx, nos: int, sides: int, mod: int):
    result = 0
    log = []
    for i in range(0, nos):
        num = randint(1, sides)
        log.append(num)
        result += num
    result += mod
    await ctx.send('{}d{} modified by {} ... {} ... You rolled {}!'.format(nos, sides, mod, log, result))

@bot.command()
async def add(ctx, a: int, b: int):
    await ctx.send(a+b)

@bot.command()
async def subtract(ctx, a: int, b: int):
    await ctx.send(a-b)

@bot.command()
async def multiply(ctx, a: int, b: int):
    await ctx.send(a*b)

@bot.command()
async def divide(ctx, a: int, b: int):
    await ctx.send(a/b)


bot.remove_command('help')

@bot.command()
async def help(ctx):
    embed = discord.Embed(title="Spoz-Bot", description="A very Spozzy Bot. List of commands are:", color=0xeee657)

    embed.add_field(name="!support", value="Totally professional technical advice", inline=False)
    embed.add_field(name="!ip", value="Connection info for the C A R T E L Project Ozone 3 Minecraft server", inline=False)
    embed.add_field(name="!roll X Y Z", value="Rolls **X** die with **Y** sides and applies a **Z** modifier to the result", inline=False)
    embed.add_field(name="!help", value="Gives this message", inline=False)

    await ctx.send(embed=embed)

bot.run('Insert Your token here')
